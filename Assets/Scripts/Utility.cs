﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour {

    public static bool AlmostEqual (Vector3 a, Vector3 b, float dist) {
        if (Vector3.Distance (a, b) <= dist)
            return true;
        return false;
    }
    public static bool AlmostEqual (float a, float b, float dist) {
        if (Mathf.Abs (a - b) <= dist)
            return true;
        return false;
    }

}
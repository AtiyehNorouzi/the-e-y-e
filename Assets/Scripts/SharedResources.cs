﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharedResources : MonoBehaviour {
	[SerializeField]
	private Light characterLight;
	[SerializeField]
	private GameObject character;
	[SerializeField]
	private Animator characterAnimator;
	[SerializeField]
	private Sprite logoSprite;
		[SerializeField]
	private Sprite emtSprite;
	private static SharedResources _instance;
	public static SharedResources instance {
		get {
			if (_instance == null)
				_instance = FindObjectOfType<SharedResources> ();

			return _instance;
		}
	}
	public Light CharacterLight {
		get {
			return characterLight;
		}
	}
	public GameObject Character {
		get {
			return character;
		}
	}

	public Animator CharacterAnimator {
		get {
			return characterAnimator;
		}
	}
	public Sprite letterLogo {
		get {
			return logoSprite;
		}
	}
		public Sprite emptySprite {
		get {
			return emtSprite;
		}
	}
}
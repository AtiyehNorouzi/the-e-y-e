﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room3Manager : MonoBehaviour {
	public AudioClip bach;
	public AudioSource source;
	public AudioSource prevSource;
	public GameObject particles;
	public Collider edgecollider;
	private static Room3Manager _instance;
	public static Room3Manager Instance {
		get {
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<Room3Manager> ();
			return _instance;
		}

	}
	public void EnableBorder () {
		edgecollider.enabled = true;
	}
	public void FinishLevel () {
		StartCoroutine (NextLevel ());
	}
	IEnumerator NextLevel () {
		yield return new WaitForSeconds (2f);
		LevelController.Instance.NextLeveL ();
		yield return new WaitForSeconds (1f);
		ItemAccess.Instance.CreateNewItem ("letter4", false, SharedResources.instance.letterLogo);
		yield return new WaitForSeconds (4f);
		SharedResources.instance.Character.GetComponent<Rigidbody> ().velocity = Vector3.zero;
		SharedResources.instance.CharacterAnimator.enabled = false;
	}
	public void FinishGame () {
		StartCoroutine (startend ());
	}
	IEnumerator startend () {
		yield return new WaitForSeconds (2.5f);
		DialogeManager.Instance.dialogeText.gameObject.SetActive (true);
		DialogeManager.Instance.ChangeDialoge (new Vector3 (-16.9f, 129f, 113.38f), "I'm in the place that an eyeball means to be \n        Now WE know where our home is . . .");
		yield return new WaitForSeconds (4.5f);
		SharedResources.instance.Character.GetComponent<Renderer> ().material.color = Color.white;
		ItemAccess.Instance.DeletePrevItems ();
		DialogeManager.Instance.dialogeText.gameObject.SetActive (false);
		prevSource.clip = bach;
		prevSource.Play ();

		LevelController.Instance.FirstLevelAgain ();
		RenderSettings.ambientIntensity = 1.6f;
		yield return new WaitForSeconds (2.5f);
		source.enabled = true;
		source.Play ();
		particles.SetActive (true);
		particles.transform.GetComponentInChildren<ParticleSystem> ().Play ();
		DialogeManager.Instance.ChangeDialoge (new Vector3 (-30.5f, 115.7f, 112.5f), "Unbelievable!\n The home that i was searching for is \n the same dark home i wish to \n escape from it ...");

		yield return new WaitForSeconds (6f);
		DialogeManager.Instance.dialogeText.gameObject.SetActive (false);
		yield return new WaitForSeconds (3f);
		DialogeManager.Instance.ShowLetter (4, true);
		yield return new WaitForSeconds (3f);
		DialogeManager.Instance.ShowLetter (4, false);
		yield return new WaitForSeconds (1.5f);
		DialogeManager.Instance.ShowLetter (5, true);
		yield return new WaitForSeconds (4f);
		DialogeManager.Instance.ShowLetter (5, false);

		ItemAccess.Instance.CreateNewItem ("letter5", false, SharedResources.instance.letterLogo);
		ItemAccess.Instance.CreateNewItem ("letter6", false, SharedResources.instance.letterLogo);

	}
}
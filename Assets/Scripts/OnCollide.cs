﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollide : MonoBehaviour {
	bool room3 = false;
	bool broken = false;
	int ballCount = 0;
	int counter = 0;
	public Sprite logo;
	private void OnTriggerEnter (Collider other) {
		if (other.gameObject.tag == "player") {
			Room1Manager.Instance.ChangeState (Room1State.GrabLamp);
			ItemAccess.Instance.CreateNewItem ("lamp", false, logo);
			gameObject.SetActive (false);
		}
	}

	void OnCollisionEnter (Collision other) {
		if (other.gameObject.tag == "player") {
			if (gameObject.tag == "broken") {
				if (Room2Manager.Instance.ballNumber >= 3)
					ballCount++;
				if (ballCount > 4 && !broken) {
					broken = true;
					ballCount = 0;
					Room2Manager.Instance.ballNumber = -1;
					DialogeManager.Instance.ChangeDialoge (new Vector3 (-26.13f, 120f, 113.38f), "Boom , it breaks...");
					Room2Manager.Instance.ChangeState (Room2State.End);
					gameObject.transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y, 0.0093f);
				}
			} else if (gameObject.tag == "room3") {
				if (!room3)
					LevelController.Instance.NextLeveL ();
				room3 = true;
			} else {
				counter++;
				if (counter == 1)
					DialogeManager.Instance.ChangeDialoge (new Vector3 (-15.85f, 115.34f, 113.38f), "There is a door here...");
				if (counter == 3) {
					GetComponent<Rigidbody> ().isKinematic = false;
					Room2Manager.Instance.ChangeState (Room2State.GrabLamp);
					Invoke ("DeactiveDoor", 3);
				}
			}

		}
	}
	void DeactiveDoor () {
		gameObject.SetActive (false);
		LevelController.Instance.NextLeveL ();
	}

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ItemAccess : MonoBehaviour {
	public Item[] items = new Item[6];
	int currentItem = 1;
	private static ItemAccess _instance;
	public static ItemAccess Instance {
		get {
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<ItemAccess> ();
			return _instance;
		}

	}
	public void CreateNewItem (string itemName, bool on, Sprite logoImage) {
		if (itemName.Length > 0)
			items[currentItem].itemName = itemName;
		items[currentItem].isUsing = on;
		items[currentItem].image.sprite = logoImage;
		if (itemName == "lamp")
			items[currentItem].ShowLampState ();
		currentItem++;
	}
	public void DeletePrevItems () {
		for (int i = 0; i < items.Length; i++) {
			items[i].isUsing = false;
			items[i].itemName = "";
			items[i].image.sprite = SharedResources.instance.emptySprite;
		}
		currentItem = 0;

	}

}
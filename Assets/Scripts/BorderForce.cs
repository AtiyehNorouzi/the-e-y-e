﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderForce : MonoBehaviour 
{
	bool firstRight = true;
	public enum EdgeType{Up =0 , Down , Right , Left , Forward , Backward };
	public EdgeType type;
	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag == "player")
		{			
			if(type == EdgeType.Up)
			{
				other.rigidbody.AddForce(transform.up*150);
			}
			else if(type == EdgeType.Down)
			{
				other.rigidbody.AddForce(-transform.up * 150);
			}
			else  if (type == EdgeType.Right)
			{
				if(firstRight)
				{
					Room1Manager.Instance.ChangeState(Room1State.ReachDoor);
					firstRight = false;
				}
				other.rigidbody.AddForce(transform.right * 100);
			}
			else if(type == EdgeType.Left)
			{
				other.rigidbody.AddForce(-transform.right * 100);
			}
			else if(type == EdgeType.Forward)
			{
				other.rigidbody.AddForce(transform.forward * 150);
			}
			else
			{
				other.rigidbody.AddForce(-transform.forward * 150);
			}
		} 

	}
}

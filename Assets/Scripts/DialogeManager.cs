﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DialogeManager : MonoBehaviour {
	public GameObject letterPanel;
	public Sprite[] letters;
	public TextMesh dialogeText;
	private static DialogeManager _instance;
	public static DialogeManager Instance {
		get {
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<DialogeManager> ();
			return _instance;
		}

	}
	public void ShowLetter (int index, bool isUsing) {
		letterPanel.SetActive (isUsing);
		if (isUsing)
			letterPanel.GetComponent<Image> ().sprite = letters[index];
	}
	// Use this for initialization
	public void ChangeDialoge (Vector3 position , string text) {
		dialogeText.text = text;
		dialogeText.transform.position = position;
		dialogeText.transform.rotation = Quaternion.identity;
		StartCoroutine (ShowText ());
	}
	IEnumerator ShowText () {
		dialogeText.gameObject.SetActive (true);
		yield return new WaitForSeconds (6);
		dialogeText.gameObject.SetActive (false);
	}
	public void EnableDialoge (bool enable) {
		dialogeText.gameObject.SetActive (enable);
	}
	
}
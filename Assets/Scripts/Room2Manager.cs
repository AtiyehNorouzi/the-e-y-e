﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Room2State { None ,GrabLamp , BallGrab , End};
public class Room2Manager : MonoBehaviour {
	public Animator brokenWalls;
	public GameObject endpanel;
	public int ballNumber=0;
	public Light light;
	public GameObject[] friends;
	private static Room2Manager _instance;
	public static Room2Manager Instance
	{
		get
		{
			if(_instance == null)
			 _instance = GameObject.FindObjectOfType<Room2Manager>();
			 return _instance;
		}

	}
	Room2State state;
	// Update is called once per frame
	public void ChangeState (Room2State state) {
		if(state == Room2State.GrabLamp)
		{
			ItemAccess.Instance.CreateNewItem("letter2" , false , SharedResources.instance.letterLogo);
		}
		else if(state == Room2State.BallGrab)
		{
			if(ballNumber > 3)
				return;
			if(ballNumber ==0)
			{
				light.enabled = true;
				light.color = Color.green;
			}
			else if(ballNumber == 1)
			{
				light.color = Color.red;
			}
			else if (ballNumber == 2)
			{
				light.color = Color.blue;
			}
			else
			{
				light.color = Color.yellow;
			}
			friends[ballNumber].SetActive(true);
			friends[ballNumber + 4].SetActive(false);
			ballNumber++;
			
		}
		else if(state == Room2State.End)
		{
			brokenWalls.enabled = true;
			brokenWalls.Play(0);
			for(int i =0;i< 4;i++)
			{
				friends[i].GetComponent<BallMove>().enabled = false;
			}
			Invoke("End" , 5f);
		}
	}
	void End(){	
		LevelController.Instance.NextLeveL();
	
	}
}

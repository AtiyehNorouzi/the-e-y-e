﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDownButtons : MonoBehaviour {
	public float downLimit = 2;
	public Vector3 dir;
	public void OnPush()
	{
		transform.position -= dir*downLimit;
		Room1Manager.Instance.CheckPattern(dir.x < 0 ? Direction.Right : dir.x > 0 ? Direction.Left : dir.y >0 ? Direction.Down : Direction.Up);
	}
	private void OnCollisionEnter(Collision other) {
			OnPush();
			Invoke("OnPull" , 2f);
	}
	public void OnPull(){
			transform.position += dir*downLimit;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour {
	bool end = false;
	bool firstTime = true;
	public float speed;
	public BoxCollider collider;
	private Rigidbody rb;
    private bool windowDisable = false;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void Update () {

		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		rb.AddForce (movement * speed);
		if (Input.GetKeyDown (KeyCode.Space))
			Jump ();
		if (Input.GetKey (KeyCode.Space)) {
			if (Input.GetKeyDown (KeyCode.A)) {
				rb.AddForce ((Vector3.up + Vector3.left) * 3, ForceMode.Impulse);
			} else if (Input.GetKeyDown (KeyCode.D)) {
				rb.AddForce ((Vector3.up + Vector3.right) * 3, ForceMode.Impulse);
			}
		}
	}
	void Jump () {
		rb.AddForce (Vector3.up * 5, ForceMode.Impulse);
	}
	private void OnCollisionEnter (Collision other) {
		if (gameObject.tag != "player")
			return;
		if (other.gameObject.tag == "room2") {
			if (transform.position.y > 121.42f)
				collider.enabled = true;
		}
		if (other.gameObject.tag == "broken") {
			if (firstTime) {
				DialogeManager.Instance.ChangeDialoge (new Vector3 (-26.13f, 120f, 113.38f), "The wall has Cracks, I must break it some how...");
				firstTime = false;
			} else if (Room2Manager.Instance.ballNumber > 0) {
				if (Room2Manager.Instance.ballNumber < 3)
					DialogeManager.Instance.ChangeDialoge (new Vector3 (-26.13f, 120f, 113.38f), "Not enough balls...");
				else {
					DialogeManager.Instance.ChangeDialoge (new Vector3 (-26.13f, 120f, 113.38f), "Together...");
				}
			} else if(Room2Manager.Instance.ballNumber  == 0) {
				DialogeManager.Instance.ChangeDialoge (new Vector3 (-26.13f, 120f, 113.38f), "Lets checkout the window...");
			}
		}
		if (other.gameObject.tag == "window") {
            if (!windowDisable)
            {
                Room2Manager.Instance.ChangeState(Room2State.BallGrab);
                windowDisable = true;
                Invoke("EnableWindow", 2f);
          
            }
	
		}
		if (other.gameObject.tag == "inroom2" && transform.position.z >= 94f) {
			other.collider.isTrigger = false;
		}
		if (other.gameObject.tag == "fanar") {

			SharedResources.instance.CharacterAnimator.enabled = true;
			SharedResources.instance.CharacterAnimator.Play (0);
			Room3Manager.Instance.FinishLevel ();

		}
		if (other.gameObject.tag == "room4") {
			if(!end)
				Room3Manager.Instance.FinishGame();
			end = true;
		}

	}
    void EnableWindow()
    {
        windowDisable = false;
    }
}
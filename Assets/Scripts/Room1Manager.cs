﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Room1State { None, GrabLamp = 0, ReachDoor, ReachGravity, ReachPaper, PressGravity, ReachPattern };
public enum Direction { Up, Down, Right, Left };

public class Room1Manager : MonoBehaviour {

 public GameObject hint;
 public GameObject edges;
 public Animator doorToOpen;
 public Light room1Light;

 Direction[] pattern = {Direction.Up, Direction.Left, Direction.Down, Direction.Right };
 int next = 0;
 public LineRenderer renderer;
 Vector3[] lineRendererTransforms;
 private static Room1Manager _instance;
 public static Room1Manager Instance {
 get {
 if (_instance == null)
 _instance = GameObject.FindObjectOfType<Room1Manager> ();
 return _instance;
		}

	}
	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Start () {
		lineRendererTransforms = new Vector3[renderer.positionCount];
		for (int i = 0; i < renderer.positionCount; i++) {
			lineRendererTransforms[i] = renderer.GetPosition (i);
		}
		renderer.positionCount = 0;

	}
	public void ChangeState (Room1State state) {

		if (state == Room1State.GrabLamp) {
			GrabLamp ();
		} else if (state == Room1State.ReachDoor) {
			ShowDialoge ();
		} else if (state == Room1State.ReachGravity) {
			hint.SetActive (true);
		} else if (state == Room1State.ReachPaper) {

		} else if (state == Room1State.PressGravity) {

		} else if (state == Room1State.ReachPattern) {
			renderer.gameObject.SetActive (true);
			StartCoroutine (ShowLineRenderer ());
			room1Light.gameObject.SetActive (true);
			edges.transform.GetChild (1).gameObject.SetActive (false);
			StartCoroutine (OpenDoor ());
		}
	}
	IEnumerator OpenDoor () {
		yield return new WaitForSeconds (2f);
		doorToOpen.enabled = true;
		doorToOpen.Play (0);
		yield return new WaitForSeconds (2f);
		LevelController.Instance.NextLeveL ();
	}
	public void CheckPattern (Direction d) {
		if (next < pattern.Length) {
			if (d == pattern[next]) {
				if (next == pattern.Length - 1)
					ChangeState (Room1State.ReachPattern);
				next++;
			} else {
				next = 0;
			}
		}

	}
	void ShowDialoge () {
		DialogeManager.Instance.ChangeDialoge (new Vector3(-24f , 112.4f , 110.106f ), "It's too dark, I think I get lost...");
        DialogeManager.Instance.ChangeDialoge(new Vector3(-24f, 112.4f, 110.106f), "I must find a way to find  my home!");
    }
	void GrabLamp () {

	}
	public void LampOn (bool enable) {
		SharedResources.instance.CharacterLight.enabled = enable;
	}
	IEnumerator ShowLineRenderer () {
		renderer.positionCount = lineRendererTransforms.Length;
		yield return new WaitForSeconds (0.5f);
		renderer.SetPosition (0, lineRendererTransforms[0]);
		yield return new WaitForSeconds (0.5f);
		renderer.SetPosition (1, lineRendererTransforms[1]);
		yield return new WaitForSeconds (0.5f);
		renderer.SetPosition (2, lineRendererTransforms[2]);
		yield return new WaitForSeconds (0.5f);
		renderer.SetPosition (3, lineRendererTransforms[3]);
		yield return new WaitForSeconds (0.5f);
		renderer.SetPosition (4, lineRendererTransforms[4]);
	}

	public void Finish () {
		renderer.gameObject.SetActive (true);
		StartCoroutine (ShowLineRenderer ());
		room1Light.gameObject.SetActive (true);
		edges.transform.GetChild (1).gameObject.SetActive (false);
		doorToOpen.Play (0);
	}
}
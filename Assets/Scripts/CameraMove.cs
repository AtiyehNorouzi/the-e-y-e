﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Rooms { None, Room1, Pele , Room2, Room3, Room4 }

public class CameraMove : MonoBehaviour {

	public float ZoomTime = 5;
	public float RotateTime = 10;
	public float[] ZoomBounds = new float[] { 10f, 85f };
	float timer = 0;
	private Vector3 lastPosition;
	private Quaternion lastRotation;
	public Camera camera;
	public Transform[] cameraPositions;
	float[] filedOfViews;

	Rooms currrentRoom = Rooms.None;
	public void ZoomIn (int room) {
		timer = 0;
		currrentRoom = (Rooms) room;

	}
	float fov;
	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Start () {
		lastPosition = camera.transform.position;
		lastRotation = camera.transform.rotation;
		fov= Camera.main.fieldOfView;

	}
	void Update () {
		if (currrentRoom != Rooms.None) {
			fov = Camera.main.fieldOfView;
			fov += Input.GetAxis("Mouse ScrollWheel") * 2f;
			fov = Mathf.Clamp(fov, 5, 11);
			Camera.main.fieldOfView = fov;
			if (!Utility.AlmostEqual (timer, 1, 0.05f)) {

				timer += Time.deltaTime / ZoomTime;
				camera.transform.position = Vector3.Lerp (lastPosition, cameraPositions[(int) currrentRoom].position, timer);
				camera.transform.rotation = Quaternion.Lerp (lastRotation, cameraPositions[(int) currrentRoom].rotation, timer);
			} 

				else {
				lastPosition = camera.transform.position;
				lastRotation = camera.transform.rotation;
			}
		}
	}

	void ZoomCamera (float offset, float speed) {
		if (offset == 0) {
			return;
		}
		camera.fieldOfView = Mathf.Clamp (camera.fieldOfView - (offset * speed), ZoomBounds[0], ZoomBounds[1]);
	}
}
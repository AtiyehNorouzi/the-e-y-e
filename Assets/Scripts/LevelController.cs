﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {
	public GameObject hidden;
	public GameObject prev;
	[System.Serializable]
	public struct LevelLight {
		public float lightIntensity;
		public Color characterAlbedo;
	}
	private static LevelController _instance;
	public static LevelController Instance {
		get {
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<LevelController> ();
			return _instance;
		}

	}

	public LevelLight[] levelLights;
	public int currentLevel = 0;
	public CameraMove cameraControl;
	// Use this for initialization
	void Start () {
		Invoke ("NextLeveL", 3f);
	}
	public void FirstLevelAgain () {
		hidden.SetActive (true);
		prev.SetActive (false);
		SharedResources.instance.Character.transform.position = new Vector3 (-22.26f, 115.55f, 95.6f);
		cameraControl.transform.position = new Vector3 (-22.6f, 133.6f, 24.2f);
		cameraControl.transform.rotation = Quaternion.Euler (12.7f, 0, 0);
	}
	public void NextLeveL () {

		if (currentLevel != 1 && currentLevel != 3) {
			int index = 0;
			if (currentLevel == 4)
				index = currentLevel - 2;
			else if (currentLevel == 2)
				index = currentLevel - 1;
			else if (currentLevel > 3)
				index = 3;
			DialogeManager.Instance.ShowLetter (index, true);
			StartCoroutine (disableLetter (index));
		}
		if (currentLevel > 3 && currentLevel != 4) {
			ItemAccess.Instance.CreateNewItem ( "letter3" , false, SharedResources.instance.letterLogo);
		}
		if (currentLevel < 2) {
			RenderSettings.ambientIntensity = levelLights[currentLevel].lightIntensity;
			SharedResources.instance.Character.GetComponent<Renderer> ().material.color = levelLights[currentLevel].characterAlbedo;
		}
		if (currentLevel == 3) {
			RenderSettings.ambientIntensity = levelLights[currentLevel - 1].lightIntensity;
			SharedResources.instance.Character.GetComponent<Renderer> ().material.color = levelLights[currentLevel - 1].characterAlbedo;
		}
		currentLevel++;
		cameraControl.ZoomIn (currentLevel);

	}
	IEnumerator disableLetter (int level) {
		yield return new WaitForSeconds (6f);
		DialogeManager.Instance.ShowLetter (level, false);
	}
	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	void Update () {
		if (Input.GetKeyDown (KeyCode.Home)) {

			if (currentLevel == 1)
				Room1Manager.Instance.Finish ();
			if (currentLevel == 3) {

			}
			if (currentLevel > 0)
				NextLeveL ();
		}
	}

}
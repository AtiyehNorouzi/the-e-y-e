﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Item : MonoBehaviour {

	public bool isUsing = false;
	public string itemName;
	public Image image;
	bool firstOn = true;
	Image parent;
	public Color disableColor;
	public Color enableColor;
	private void Start () {
		image = GetComponent<Image> ();
		parent = image.transform.parent.GetComponent<Image> ();
	}
	public void ChangeEnable () {
		isUsing = !isUsing;
		if (itemName == "lamp")
			ShowLampState ();
		else if (itemName == "letter1") {
			DialogeManager.Instance.ShowLetter (0, isUsing);
			enableUI ();
		} else if (itemName == "letter2") {
			DialogeManager.Instance.ShowLetter (1, isUsing);
			enableUI ();
		} else if (itemName == "letter3") {
			DialogeManager.Instance.ShowLetter (2, isUsing);
			enableUI ();
		} else if (itemName == "letter4") {
			DialogeManager.Instance.ShowLetter (3, isUsing);
			enableUI ();
		}
		else if (itemName == "letter5") {
			DialogeManager.Instance.ShowLetter (4, isUsing);
			enableUI ();
		}
		else if (itemName == "letter6") {
			DialogeManager.Instance.ShowLetter (5, isUsing);
			enableUI ();
		}

	}
	void enableUI () {

		if (isUsing)
			parent.color = enableColor;
		else
			parent.color = disableColor;
	}
	public void ShowLampState () {
		Room1Manager.Instance.LampOn (isUsing);
		if (isUsing) {
			if (firstOn == true) {
				Room1Manager.Instance.ChangeState (Room1State.ReachGravity);
				firstOn = false;
			}
			parent.color = enableColor;
		} else
			parent.color = disableColor;
	}
}